UI Inventory
============

[![status-badge](https://ci.codeberg.org/api/badges/13378/status.svg)](https://ci.codeberg.org/repos/13378)

This is the web based user interface for [Open
Inventory](https://codeberg.org/eotl/open-inventory). It is currently a Vue2
app. It is automatically installed in *Open Inventory* when it is deployed. Each
stable version is published on NPM as `@eotl/ui-inventory` package.


## Developing

**Important:**
This setup assumes the presence of open-inventory in a parallel directory (both `ui-inventory`, and `open-inventory` must be in the same directory.)
This is needed to be able to update the UI files in the `open-inventory/public`, each time there is a change in `ui-inventory`.


Our recommended method of developing `ui-inventory` is using Docker. There are helper 
commands in the `Makefile` to run various Docker operations.

The first time setting up your environment run:

```
$ make first-run
```

Next time, just start Docker and run the dev server with:

```
$ make dev
```

To upgrade or install new NPM packages:

```
$ make up
$ make packages
```

To do anything else in the user or root shell such as install OS depedencies:

```
$ make shell
$ make shell-root
```


### Develop UI with an instance of `open-inventory`

To see your changes live in the running instance of `open-inventory`, make sure that `make dev` is running within `ui-inventory`.

How is that working under the hood? `open-inventory/public` is mounted as volume within `ui-inventory/docker-compose.yml`.


## Developing without Docker

If you prefer to not use Docker you can run things manually. You need a locally
installed Node JS environment and `yarn` package manager. 

Install the packages:

```
$ yarn
```

Run local server:

```
$ yarn serve
```

Build files for production use or new package

```
$ yarn build
```

The compiled app files will be in the `./open-inventory-public/` directory. Please make sure the directory has appropriate permissions.
