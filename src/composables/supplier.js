/* Supplier helpers */

export function supplierName(suppliers, supplierId) {
    let name = "Supplier: " + supplierId

    for (var key in suppliers) {
        if (suppliers[key].id == supplierId) {
            name = suppliers[key].name
        }
    }

    return name
}
