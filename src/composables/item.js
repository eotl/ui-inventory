/* Item helpers */

export function itemName(item) {
    if (item.name) {
        return item.name
    } else {
        return item.id
    }
}
