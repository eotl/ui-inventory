/* Currency related things */

export function currencySymbol(currencies, key) {
    let symbol = key 
    if (key in currencies) {
        symbol = currencies[key].symbol
    }

    return symbol
}
