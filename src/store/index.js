import { defineStore } from 'pinia'
import { Currency, Details, Inventory, Languages, Licenses, Measurements } from '@eotl/core/enums'
import { useEotlCore } from '@eotl/core/store'

const item = {
    id: '',
    name: '',
    categoryId: '',
    measurement: 'item',
    units: 1,
    details: [],
    inventories: [],
    suppliers: [],
    translations: [],
    isPublic: 1
}

const itemDetails = {
    sustainability: [],
    transparency: [],
    allergens: [],
    dietary: [],
    description: '',
    license: 'na',
    produced_in: 'unknown'
}

const inventory = {
    itemId: '',
    supplierId: 'unknown',
    measurement: 'item',
    units: 1,
    price: 0.00,
    currency: window.inventoryConfig.currency,
    share: ''
}

const supplier = {
    id: '',
    name: '',
    description: '',
    street: '',
    building: '',
    postalCode: '',
    city: '',
    latitude: 0.00,
    longitude: 0.00,
    picture: '',
    active: 1,
    itemsVisibility: "all",
    inventoriesVisibility: "all",
    zoneId: window.inventoryConfig.zones_parent.zone
}

const hours = {
    monday:    { opens: '09:00', closes: '18:00', raw: {} },
    tuesday:   { opens: '09:00', closes: '18:00', raw: {} },
    wednesday: { opens: '09:00', closes: '18:00', raw: {} },
    thursday:  { opens: '09:00', closes: '18:00', raw: {} },
    friday:    { opens: '09:00', closes: '18:00', raw: {} },
    saturday:  { opens: '09:00', closes: '18:00', raw: {} },
    sunday:    { opens: '09:00', closes: '18:00', raw: {} }
}

const supplierDetails = {
    phone: '',
    email: '',
    website: '',
    note: ''
}

export const useStore = defineStore('@eotl/ui-inventory', {
    state: () => ({
        config: window.inventoryConfig,
        languages: Languages,
        currencies: Currency,
        groups: Inventory,
        measurements: Measurements,
        details: Details,
        licenses: Licenses,
        defaults: {
            item: item,
            itemDetails: itemDetails,
            inventory: inventory,
            supplier: supplier,
            supplierHours: hours,
            supplierDetails: supplierDetails
        },
        alert: {
            show: false,
            style: 'primary',
            title: 'Alert',
            message: 'This is a default alert message!'
        },
        alerts: [],
        categories: [],
        zones: [],
        suppliers: [],
        changes: []
    }),
    actions: {
        merge(newState) {
            for (let key in newState) {
                if (key in this) {
                    this[key] = newState[key]
                }
            }
        },
        registerAlerts() {
            const eotlStore = useEotlCore()
            if (this.alerts.length > 0) {
                console.log('Show alerts', this.alerts)
                eotlStore.alertWarning(JSON.stringify(this.alerts))
            }
        },
        async loadZones() {
            const eotlStore = useEotlCore()
            const response = await eotlStore.fetchGet('/zones')

            if (response.status == 'success') {
                this.merge({
                    zones: { ...this.zones, ...response.data },
                })
            }
        },
        async fetchCategories() {
            const eotlStore = useEotlCore()
            const response = await eotlStore.fetchGet('/categories')

            if (response.status == 'success') {
                this.merge({
                    categories: response.data
                })
            }
        },
        async fetchSuppliers() {
            const eotlStore = useEotlCore()
            const response = await eotlStore.fetchGet('/suppliers')

            if (response.status == 'success') {
                this.merge({
                    suppliers: response.data
                })
            }
        }
    }
})
