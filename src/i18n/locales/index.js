import en from './en.js'
import de from './de.js'

export const messages = {
    en,
    de,
}