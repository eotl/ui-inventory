import { createI18n } from 'vue-i18n';
import * as messages from './locales'

const i18n = createI18n({
  locale: "en", // set locale
  messages, // set locale messages
  silentFallbackWarn: true
})

export default i18n;
