import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
import i18n from './i18n'
import jQuery from 'jquery'
import bootstrap from 'bootstrap'

import { useEotlCore } from '@eotl/core/store'
import { useStore } from './store'

window.jQuery = jQuery
window.$ = jQuery

const app = createApp(App)

const pinia = createPinia()

app.use(router).use(pinia).use(i18n)
useEotlCore().init(useStore().config)

app.mount('#ui-inventory')
