import { nextTick } from 'vue'
import { createWebHashHistory, createRouter } from 'vue-router'
import { createRoutes } from './routes'

const DEFAULT_TITLE = 'Open Inventory'

const router = createRouter({
  routes: createRoutes(),
  history: createWebHashHistory()
})

router.afterEach((to) => {
  nextTick(() => {
    document.title = to.meta.title || DEFAULT_TITLE
  })
})

export default router;
