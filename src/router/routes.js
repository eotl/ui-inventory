export const createRoutes = (pathPrefix = '/') => [
    {
        path: `${pathPrefix}`,
        component: () => import('@/views/Home.vue'),
        name: "inventory:home",
        meta: { title: 'Home' }
    }, {
        path: `${pathPrefix}items`,
        name: "inventory:items",
        component: () => import('@/views/Items.vue'),
        props: true,
        meta: { title: 'Items' }
    }, {
        path: `${pathPrefix}items/:id`,
        name: 'inventory:item-manage',
        component: () => import('@/views/ItemManage.vue'),
        props: true,
        meta: { title: 'Manage Item' },
        children: [{
            name: 'inventory:item-create',
            path: '',
            component: () => import('@/components/ItemBasics')
        }, {
            name: 'inventory:item-basics',
            path: 'basics',
            component: () => import('@/components/ItemBasics')
        }, {
            name: 'inventory:item-details',
            path: 'details',
            component: () => import('@/components/ItemDetails')
        }, {
            name: 'inventory:item-translations',
            path: 'translations',
            component: () => import('@/components/ItemTranslations')
        }, {
            name: 'inventory:item-media',
            path: 'media',
            component: () => import('@/components/ItemMedia')
        }, {
            name: 'inventory:item-inventory',
            path: 'inventory',
            component: () => import('@/components/ItemInventory')
        }, {
            name: 'inventory:item-inventory-edit',
            path: 'inventory/:supplierId',
            component: () => import('@/components/ItemInventoryEdit')
        }, {
            name: 'inventory:item-packaging',
            path: 'packaging',
            component: () => import('@/components/ItemPackaging')
        }, {
            name: 'inventory:item-data',
            path: 'data',
            component: () => import('@/components/ItemData')
        }]
    }, {
        path: `${pathPrefix}bundles`,
        name: "inventory:bundles",
        component: () => import('@/views/Bundles.vue'),
        meta: { title: 'Bundles' }
    }, {
        path: `${pathPrefix}bundles/create`,
        name: 'inventory:bundle-create',
        component: () => import('@/views/BundleCreate.vue'),
        meta: { title: 'Create Bundle' }
    }, {
        path: `${pathPrefix}bundles/:id`,
        name: 'inventory:bundle-manage',
        component: () => import('@/views/BundleManage.vue'),
        meta: { title: 'Manage Bundles' },
    }, {
        path: `${pathPrefix}suppliers`,
        name: 'inventory:suppliers',
        component: () => import('@/views/Suppliers.vue'),
        meta: { title: 'Suppliers' }
    }, {
        path: `${pathPrefix}suppliers/create`,
        name: 'inventory:supplier-create',
        component: () => import('@/views/SupplierCreate.vue'),
        meta: { title: 'Create Supplier' }
    }, {
        path: `${pathPrefix}suppliers/:id`,
        name: 'inventory:supplier-manage',
        component: () => import('@/views/SupplierManage.vue'),
        meta: { title: 'Manage Supplier' }
    }, {
        path: `${pathPrefix}categories`,
        name: 'inventory:categories',
        component: () => import('@/views/Categories.vue'),
        meta: { title: 'Categories' }
    }, {
        path: `${pathPrefix}categories/create`,
        name: 'inventory:category-create',
        component: () => import('@/views/CategoryCreate.vue'),
        meta: { title: 'Create Category' }
    }, {
        path: `${pathPrefix}categories/:id`,
        name: 'inventory:category-manage',
        component: () => import('@/views/CategoryManage.vue'),
        meta: { title: 'Manage Category' }
    }, {
        path: '/:catchAll(.*)',
        name: 'inventory:not-found',
        component: () => import('@/views/NotFound.vue'),
        meta: { title: 'Not Found' }
    }
];
