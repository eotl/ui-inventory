module.exports = {
    env: {
        browser: true,
        es2021: true,
        node: true,
    },
    extends: ["eslint:recommended", "plugin:vue/vue3-recommended"],
    parser: "vue-eslint-parser",
    parserOptions: {
        ecmaVersion: 12,
        sourceType: "module",
    },
    plugins: ["vue"],
    rules: {
        "indent": ["warn", 4],
        "vue/no-v-html": "off",
        "vue/multi-word-component-names": "off",
        "no-unused-vars": [
            "warn",
            { vars: "all", args: "after-used", ignoreRestSiblings: false },
        ],
        "vue/no-unused-components": ["warn", {
            "ignoreWhenBindingPresent": true
        }],
        semi: ["warn", "never"],
        "vue/html-indent": ["error", 4, {
            "ignores": ["VAttribute"]
        }],
        "vue/max-attributes-per-line": ["warn", {
            "singleline": {
                "max": 8
            },
            "multiline": {
                "max": 3
            }
        }],
        "vue/first-attribute-linebreak": "off",
        "vue/html-self-closing": [0],
    },
};
