import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import { viteStaticCopy } from 'vite-plugin-static-copy'

// https://vitejs.dev/config/
export default defineConfig({
    define: {
      'process.env.NODE_ENV': JSON.stringify('production'),
    },
    plugins: [
        viteStaticCopy({
            targets: [
                { src: path.resolve(__dirname, 'node_modules/@eotl/icons/dist/*'), dest: 'images/icons' },
                { src: path.resolve(__dirname, 'node_modules/@eotl/theme-bootstrap/dist/fonts/*'), dest: 'fonts' },
                { src: path.resolve(__dirname, 'node_modules/@eotl/theme-bootstrap/dist/js/*'), dest: 'js' },
                { src: path.resolve(__dirname, 'node_modules/@eotl/theme-bootstrap/dist/css/*'), dest: 'css' },
            ],
        }),
        vue(),
    ],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src'),
        },
        extensions: ['.js', '.vue'],
    },
    build: {
        outDir: '/open-inventory-public',
        emptyOutDir: true,
        sourcemap: true,
        minify: false,
        lib: {
            entry: path.resolve(__dirname, 'src/main.js'),
            name: 'ui-inventory',
        },
    },
    server: {
        port: 8080,
        host: '0.0.0.0',
        build: {
            sourcemap: true,
            chunkSizeWarningLimit: 25000,
            rollupOptions: {
                output: {
                    manualChunks: {
                        libs: ['vue'],
                    },
                },
            },
        },
    },
})
