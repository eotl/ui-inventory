const app = Vue.createApp({
    template:
        /*html*/
        `
        <div class="flex-container" style="display: flex; justify-content: center; align-items: center">
        <!-- form header -->
        <div class="form-header-grid-container" style="padding-right: 10px; padding-left: 10px;">
            <div class="grid-header-item" id="header">Order form</div>
            <div class="grid-header-item" id="logo">E O <br> T L</div>
            <div class="grid-header-item" id="form-num">Form <div id="dynamic-form-num">Form Num</div>
            </div>
            <div class="grid-header-item" id="type">Type</div>
            <div class="grid-header-item" id="date">Date</div>
            <div class="grid-header-item" id="nickname">Nickname</div>
            <div class="grid-header-item-spacer"></div>
        </div>
    </div>
    <div class="flex-container" style="display: flex; justify-content: center; align-items: center">
        <!-- form body -->
        <div class="form-body-grid-container" style="padding-right: 10px; padding-left: 10px;">
            <!-- options -->
            <div class="grid-body-item" id="options">OPTIONS</div>
            <div class="grid-body-item" id="options-1">1</div>
            <div class="grid-body-item" id="options-2">2</div>
            <div class="grid-body-item" id="options-3">3</div>
            <div class="grid-body-item" id="options-4">4</div>
            <div class="grid-body-item" id="options-wish-note">WISH/NOTE</div>
            <div class="grid-body-item" id="options-cost">COST</div>
            <!-- end options -->

            <div class="grid-body-item" style="background-color: black; color: white;">PANTRY</div>
            <div class="grid-body-item">PASTA</div>
            <div class="grid-body-item">RICE</div>
            <div class="grid-body-item">NUTS</div>
            <div class="grid-body-item">SEEDS</div>
            <div class="grid-body-item">LEGUMES</div>
            <div class="grid-body-item">OILS</div>
            <div class="grid-body-item">SWEETNER</div>

            <div class="grid-body-item" style="background-color: black; color: white;">CLEANING</div>
            <div class="grid-body-item">DISHES</div>
            <div class="grid-body-item">LAUNDRY</div>
            <div class="grid-body-item">FLUIDS</div>
            <div class="grid-body-item">SUPPLIES</div>

            <div class="grid-body-item" style="background-color: black; color: white;">APOTHECARY</div>
            <div class="grid-body-item">HAIR</div>
            <div class="grid-body-item">BODY</div>
            <div class="grid-body-item">DENTAL</div>
            <div class="grid-body-item">SUPPLIES</div>
            <div class="grid-body-item">TOTAL</div>

            <div class="grid-body-item" id="spacer"></div>
        </div>
    </div>
        `,
    data: function () {
        return {
            title: "EOTL ORDER FORM",
            items: [],
            img: "/assets/img/",
            note: "only if its bio"
        }
    },
    methods: {
        mounted() {
            fetch("https://inventory.eotl.supply/api/items/category/mixes")
                .then(response => response.json())
                .then((data) => {
                    this.items = data;
                })
        }
    },
    computed: {
        itemId() {
            return this.items[this.updatedItem].id
        },
        itemsItem() {
            return this.items[this.updatedItem].item
        },
        itemCategoryId() {
            return this.items[this.updatedItem].categoryId
        },
        itemMeasurement() {
            return this.items[this.updatedItem].measurement
        },
        itemUnit() {
            return this.items[this.updatedItem].units
        }

    }

})
